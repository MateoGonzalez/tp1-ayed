#include <iostream>
#include<string>
#include <cstdlib>
#include "../include/utils.h"
#include "../include/RepartidorArchivo.h"
#include "../include/Pedido.h"

// Macro para detectar el sistema operativo
#ifdef _WIN32
#define OS_NAME "Windows"
#elif __linux__
#define OS_NAME "Linux"
#else
#define OS_NAME "Desconocido"
#endif

#include <regex>

using namespace std;

bool validarFormatoPatente(string cadena)
{
    regex patente2("^([a-zA-Z])([a-zA-Z])-(\\d)(\\d)(\\d)-([a-zA-Z])([a-zA-Z])$");
    regex patente1("^([a-zA-Z])([a-zA-Z])([a-zA-Z])-(\\d)(\\d)(\\d)$");
    if (regex_match(cadena, patente2) || regex_match(cadena, patente1))
        return true;
    return false;
}

bool charIsNum(string x)
{

    for (int i = 0; i < 10; i++)
    {
        string index_char = to_string(i);

        if (x == index_char)
            return true;
    }

    return false;
}


bool isNumber(string& str){
    for (char c : str)
    {
        if (!isdigit(c))
        {
            return false;
        }
    }
    return true;
}

bool isNumOrFloat(string str){
    istringstream ss(str);
    double num;
    ss >> num;
    
    return !ss.fail() && ss.eof();
}

void inicializarMatrizRepartidores(Repartidor matrizRepartidoresZona[][80], int filas, int colum)
{
    for (int i = 0; i < filas; i++)
    {
        for (int j = 0; j < colum; j++)
        {
            matrizRepartidoresZona[i][j].nombre = "";
            matrizRepartidoresZona[i][j].apellido = "";
            matrizRepartidoresZona[i][j].transporte = "";
            matrizRepartidoresZona[i][j].dni = 0;
            matrizRepartidoresZona[i][j].patente = "";
        }
    }
}

bool returnExisteRepartidorConZonaYTransporte(Repartidor matrizRepartidores[][80], int zona, string transporte){
    Repartidor repartidor;
    for (int i = 0; i < 80; i++)
    {
        repartidor = matrizRepartidores[zona-1][i];
        if(repartidor.transporte == transporte){
            return true;
        }      
    }
    return false;
}

Repartidor returnRepartidorByDni(Repartidor matrizRepartidores[][80], unsigned dni){
    Repartidor repartidor;
    for (int i = 0; i < 14; i++)
    {
        for (int j = 0; j < 80; j++)
        {
            if (matrizRepartidores[i][j].dni == dni){
                repartidor = matrizRepartidores[i][j];
                return repartidor;
            }
            
        }
    }
    return repartidor;
}

int returnZonaRepartidorByDni(Repartidor matrizRepartidores[][80], unsigned dni){
    for (int i = 0; i < 14; i++)
    {
        for (int j = 0; j < 80; j++)
        {
            if (matrizRepartidores[i][j].dni == dni){
                return i+1;
            }         
        }
    }
    return 0;
}

bool returnRepartidorExisteDni(Repartidor matrizRepartidores[][80], int filas, int colum, unsigned dni)
{

    for (int i = 0; i < filas; i++)
    {
        for (int j = 0; j < colum; j++)
        {
            if (matrizRepartidores[i][j].dni == 0)
                break;
            else if (matrizRepartidores[i][j].dni == dni)
                return true;
        }
    }
    return false;
}

void verMatriz(Repartidor matrizRepartidores[][80], int filas, int colum)
{
    int count = 0;
    for (int i = 0; i < filas; i++)
    {
        for (int j = 0; j < colum; j++)
        {
            if (matrizRepartidores[i][j].dni == 0)
                break;
            count += 1;
            cout << "zona: " << i + 1 << endl;
            cout << matrizRepartidores[i][j].dni << endl;
            cout << matrizRepartidores[i][j].nombre << endl;
            cout << matrizRepartidores[i][j].apellido << endl;
            cout << matrizRepartidores[i][j].transporte << endl;
            cout << matrizRepartidores[i][j].patente << endl
                 << endl;
        }
    }
    cout << "cant repartidores: " << count;
}

void clearTerminal()
{
    if (OS_NAME == "Linux")
        system("clear");
    else
        system("cls");
}

bool returnEspacioDisponible(Repartidor matrizRepartidores[][80], int colum, int zona, string transporte)
{

    int cantTransporte = 0;

    for (int i = 0; i < colum; i++)
    {
        if (matrizRepartidores[zona - 1][i].transporte == transporte)
        {
            cantTransporte += 1;
        }
    }

    if (cantTransporte < 20)
        return true;
    else
        return false;
}

void agregarRepartidorMatriz(Repartidor matrizRepartidores[][80], Repartidor repartidor, int colum, int zona)
{
    for (int i = 0; i < colum; i++)
    {
        if (matrizRepartidores[zona - 1][i].dni == 0)
        {
            matrizRepartidores[zona - 1][i] = repartidor;
            return;
        }
    }
}


string returnTransporteByVolumenPedido(float volumenPedido){

    if(volumenPedido < 0.005) return "moto";
    else if(volumenPedido >= 0.005 && volumenPedido <= 0.02) return "auto";
    else if(volumenPedido > 0.02 && volumenPedido <= 8) return "camioneta";
    else return "camion";
    
}



NodoListaRepartidor* sacarPrimerNodoRepartidor(NodoListaRepartidor*& primerNodoRepartidor) {

    NodoListaRepartidor* repartidorADesencolar = primerNodoRepartidor;
    
    primerNodoRepartidor = primerNodoRepartidor->proxNodoRepartidor;
   
    return repartidorADesencolar;
}


NodoListaPedido* sacarPrimerNodoPedidoDeListaDeRepartidor(NodoListaPedido*& primerNodoPedido) {

    NodoListaPedido* pedidoADesencolar = primerNodoPedido;
    
    primerNodoPedido = primerNodoPedido->proxNodoPedido;
   
    return pedidoADesencolar;
}



/* bool returnPatenteExiste(Repartidor matrizRepartidores[][80], int filas, int colum, string patente){

    for (int i = 0; i < filas; i++)
    {
        for (int j = 0; j < colum; j++)
        {
            if (matrizRepartidores[i][j].patente == "")
                break;
            else if (matrizRepartidores[i][j].patente == patente)
                return true;
        }
    }
    return false;
} */

/* void ordenarMatrizRepartidoresPorDni(Repartidor matrizRepartidores[][80], int filas, int colum){
    bool termino;
    bool arrayTerminado;
    for (int i = 0; i < filas; i++)
    {
        termino = false;
        while (!termino)
        {
            arrayTerminado = true;
            for (int j = 0; j < colum - 1; j++)
            {
                if(matrizRepartidores[i][j].dni == 0 || matrizRepartidores[i][j+1].dni == 0) break;
                if (matrizRepartidores[i][j].dni > matrizRepartidores[i][j + 1].dni)
                {
                    Repartidor temp = matrizRepartidores[i][j];
                    matrizRepartidores[i][j] = matrizRepartidores[i][j + 1];
                    matrizRepartidores[i][j+1] = temp;
                    arrayTerminado = false;
                }
            }
            if (arrayTerminado == true) termino = true;
        }
    }
} */