#include <iostream>
#include "../include/arbolComercios.h"
#include "../include/Pedido.h"
#include "../include/utils.h"
using namespace std;

void copiarNodoStackRepartidor(NodoListaRepartidor*& buffer, NodoListaRepartidor*& copia){
    buffer->dniRepartidor = copia->dniRepartidor;
    buffer->primerNodoPedido = copia->primerNodoPedido;
    buffer->proxNodoRepartidor = copia->proxNodoRepartidor;
}

void recorrerInOrder(NodoComercio* raiz) {
    if (raiz != NULL) {
        recorrerInOrder(raiz->izq);
        cout<<"Codigo: "<<raiz->comercio.codigo<< ", Cantidad de entregas: " << raiz->comercio.cantidad << endl;
        recorrerInOrder(raiz->der);
    }
}

void insertarCodigoComercio(NodoComercio*& raiz, int codigoComercio){
    if (raiz == NULL) {
        raiz = new NodoComercio();
        raiz->comercio.codigo = codigoComercio;
        raiz->comercio.cantidad = 1;
        raiz->izq = NULL;
        raiz->der = NULL;
    } else if (codigoComercio < raiz->comercio.codigo) {
        insertarCodigoComercio(raiz->izq, codigoComercio);
    } else if (codigoComercio > raiz->comercio.codigo) {
        insertarCodigoComercio(raiz->der, codigoComercio);
    } else {
        raiz->comercio.cantidad++;
    }
}


void crearArbolComercios(NodoListaRepartidor*& primerNodoRepartidor){

    cout<<"---------COMERCIOS---------"<<endl<<endl;

    if(primerNodoRepartidor == NULL){
        cout<< "No hay comercios que hayan hecho entregas"<<endl<<endl;
        return;
    }

    NodoComercio* raizNodoComercio = NULL;

    NodoListaRepartidor* nodoRepartidorADesapilar = new NodoListaRepartidor();
    copiarNodoStackRepartidor(nodoRepartidorADesapilar, primerNodoRepartidor);

    NodoListaRepartidor* nodoRepartidor = NULL;

    while (nodoRepartidorADesapilar != NULL) {
        nodoRepartidor = sacarPrimerNodoRepartidor(nodoRepartidorADesapilar);

        NodoListaPedido* nodoPedidoADesapilar = new NodoListaPedido();
        nodoPedidoADesapilar->pedido = nodoRepartidor->primerNodoPedido->pedido;
        nodoPedidoADesapilar->proxNodoPedido = nodoRepartidor->primerNodoPedido->proxNodoPedido;
       
        NodoListaPedido* nodoPedido = NULL;

        while(nodoPedidoADesapilar != NULL){
            nodoPedido = sacarPrimerNodoPedidoDeListaDeRepartidor(nodoPedidoADesapilar);

            insertarCodigoComercio(raizNodoComercio, nodoPedido->pedido.codigoComercio);
        }

    }

    

    recorrerInOrder(raizNodoComercio);

}




