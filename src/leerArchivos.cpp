#include "../include/Repartidor.h"
#include "../include/abrirArchivos.h"
#include "../include/leerArchivos.h"
#include "../include/Repartidor.h"
#include "../include/RepartidorArchivo.h"
#include "../include/utils.h"
#include <cstring>

void leerArchivos(Repartidor matrizRepartidores[][80])
{
    for (int p = 0; p < 14; p++)
        for (int i = 0; i < 80; i++)
            matrizRepartidores[p][i].dni = 0;
   
    FILE *RAuto = abrirArchivos("r", "auto");
    FILE *RCamion = abrirArchivos("r", "camion");
    FILE *RCamioneta = abrirArchivos("r", "camioneta");
    FILE *RMoto = abrirArchivos("r", "moto");

    FILE *fileArrays[3];
    fileArrays[0] = RAuto;
    fileArrays[1] = RCamion;
    fileArrays[2] = RCamioneta;
    fileArrays[3] = RMoto;

    string transporteArray[4];
    transporteArray[0] = "auto";
    transporteArray[1] = "camion";
    transporteArray[2] = "camioneta";
    transporteArray[3] = "moto";

    if (!RCamioneta || !RMoto || !RCamion || !RAuto)
        cout << "Error: Ocurrio un error al abrir los archivos"<<endl<<endl;
    else
    {
        RepartidorArchivo e;
        FILE *file;
        Repartidor d;

        for (int k = 0; k < 4; k++)
        {
            fread(&e, sizeof(struct RepartidorArchivo), 1, fileArrays[k]);
            while (!feof(fileArrays[k]))
            {
                d.apellido = e.apellido;
                d.nombre = e.nombre;
                d.dni = e.dni;
                d.patente = e.patente;
                d.transporte = transporteArray[k];
                agregarRepartidorMatriz(matrizRepartidores, d, 80, e.zona );
                fread(&e, sizeof(struct RepartidorArchivo), 1, fileArrays[k]);
            }
        }
        fclose(RAuto);
        fclose(RCamion);
        fclose(RCamioneta);
        fclose(RMoto);
    }
}