#include <iostream>
#include "../include/Repartidor.h"
#include "../include/escribirArchivos.h"
#include "../include/utils.h"
#include "../include/abrirArchivos.h"
#include "../include/RepartidorArchivo.h"
#include <cstring>
#include <bitset>

using namespace std;

FILE *getVehiculo(string, FILE *, FILE *, FILE *, FILE *);

void ordenarVectorPorDni(RepartidorArchivo[280], int);
void llenarYOrdenarVector(Repartidor[][80], RepartidorArchivo[280], string);
int returnPosicionTransporte(string);
void agregarAVectorPorTransporte(RepartidorArchivo[280], int, Repartidor, int);
void inicializarVectorRepartidoresPorTransporte(RepartidorArchivo[280], int);
// FILE *getVehiculo(string, FILE *, FILE *, FILE *, FILE *);

// void crearArchivos();
// void borrarDataArchivos();

void escribirArchivos(Repartidor matrizRepartidores[][80], int zona, string transporte)
{

    RepartidorArchivo vectorRepartidoresPorTransporte[280];
    inicializarVectorRepartidoresPorTransporte(vectorRepartidoresPorTransporte, 280);

    llenarYOrdenarVector(matrizRepartidores, vectorRepartidoresPorTransporte, transporte);

    FILE *archivo = abrirArchivos("wb", transporte); // esta línea fue cambiada, no se sabe si funciona correctamente.
    if (!archivo)
        cout << "no abrio el archivo" << endl;
    for (int i = 0; i < 280; i++)
    {
        if (vectorRepartidoresPorTransporte[i].dni != 0)
        {
            RepartidorArchivo add;
            add.zona = vectorRepartidoresPorTransporte[i].zona;
            add.dni = vectorRepartidoresPorTransporte[i].dni;
            strcpy(add.nombre, vectorRepartidoresPorTransporte[i].nombre);
            strcpy(add.apellido, vectorRepartidoresPorTransporte[i].apellido);
            strcpy(add.patente, vectorRepartidoresPorTransporte[i].patente);
            fwrite(&add, sizeof(add), 1, archivo);
            // fwrite("\n", sizeof(char), 1, archivo);
        }
    }

    fclose(archivo);
}

void inicializarVectorRepartidoresPorTransporte(RepartidorArchivo vectorRepartidores[280], int size)
{

    for (int i = 0; i < size; i++)
    {
        vectorRepartidores[i].zona = 0;
        vectorRepartidores[i].nombre[0] = '\0';
        vectorRepartidores[i].apellido[0] = '\0';
        vectorRepartidores[i].dni = 0;
        vectorRepartidores[i].patente[0] = '\0';
    }
}

void agregarAVectorPorTransporte(RepartidorArchivo vectorRepartidoresPorTransporte[280], int colum, Repartidor repartidor, int zona)
{

    for (int i = 0; i < colum; i++)
    {
        if (vectorRepartidoresPorTransporte[i].dni == 0)
        {
            vectorRepartidoresPorTransporte[i].dni = repartidor.dni;
            strcpy(vectorRepartidoresPorTransporte[i].nombre, &repartidor.nombre[0]);
            strcpy(vectorRepartidoresPorTransporte[i].apellido, &repartidor.apellido[0]);
            strcpy(vectorRepartidoresPorTransporte[i].patente, &repartidor.patente[0]);
            vectorRepartidoresPorTransporte[i].zona = zona;

            break;
        }
    }
}

void ordenarVectorPorDni(RepartidorArchivo vectorPorTransporte[280], int size)
{

    bool termino = false;
    bool arrayTerminado;

    if (vectorPorTransporte[1].dni == 0)
        return;

    while (!termino)
    {
        arrayTerminado = true;
        for (int i = 0; i < size - 1; i++)
        {
            if (vectorPorTransporte[i].dni == 0 || vectorPorTransporte[i + 1].dni == 0)
                break;
            else if (vectorPorTransporte[i].dni > vectorPorTransporte[i + 1].dni)
            {
                RepartidorArchivo temp = vectorPorTransporte[i];
                vectorPorTransporte[i] = vectorPorTransporte[i + 1];
                vectorPorTransporte[i + 1] = temp;
                arrayTerminado = false;
            }
        }
        if (arrayTerminado == true)
            termino = true;
    }
}

void llenarYOrdenarVector(Repartidor matrizRepartidores[][80], RepartidorArchivo vectorRepartidoresPorTransporte[280], string transporte)
{

    for (int i = 0; i < 14; i++) 
    {
        for (int j = 0; j < 80; j++)
        {
            if (matrizRepartidores[i][j].transporte == transporte)
            {
                agregarAVectorPorTransporte(vectorRepartidoresPorTransporte, 280, matrizRepartidores[i][j], i + 1);
            }
        }
    }

    ordenarVectorPorDni(vectorRepartidoresPorTransporte, 280);
}