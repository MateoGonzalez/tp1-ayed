#include <iostream>
#include "../include/pantalla.h"
#include "../include/Repartidor.h"
#include "../include/ingresoRepartidores.h"
#include "../include/utils.h"
#include "../include/informarRepartidores.h"
#include "../include/transportesNoDisp.h"
#include "../include/zonasConMasRepart.h"
#include "../include/asignarPedidos.h"
#include "../include/mostrarPedidos.h"
#include "../include/pedidosEnEspera.h"
#include "../include/Pedido.h"
#include "../include/arbolComercios.h"
#include "../include/pedidosNoEntregados.h"

#include <limits>

using namespace std;

#include <string>

void mostrarCantidadDePedidosEnEspera(NodoListaPedido* primerNodoPedido){
    int count = 0;

    if(primerNodoPedido != NULL){
       
        while(primerNodoPedido != NULL){
            count++;
            primerNodoPedido = primerNodoPedido->proxNodoPedido;
        }
    }
    
    cout<<"Cantidad de pedidos en espera: "<< count<<endl<<endl;
}



void pantalla(Repartidor matrizRepartidores[][80], NodoListaPedido*&primerNodoPedido, NodoListaRepartidor*& primerNodoRepartidor){

    string input = "";
    int opcion = 0;

    while(opcion != 9){
        cout<<endl<<"--------- SISTEMA DE REPARTIDORES ------------"<<endl<<endl;
        mostrarCantidadDePedidosEnEspera(primerNodoPedido);
        cout<<"1 - Ingresar repartidor"<<endl;
        cout<<"2 - Informar repartidores por zona"<<endl;
        cout<<"3 - Informar transporte no disponible"<<endl;
        cout<<"4 - Informar zona con mas repartidores"<<endl;
        cout<<"5 - Ingresar pedidos"<<endl;
        cout<<"6 - Asignar pedido a un repartidor"<<endl;
        cout<<"7 - Mostrar repartidores que hicieron entregas"<<endl;
        cout<<"9 - Cerrar"<<endl<<endl;

        cin >> input;

        if (isNumber(input) && input.length() < 2)
            opcion = stoi(input);

        clearTerminal();

        switch (opcion)
        {
        case 1:
            ingresoRepartidores(matrizRepartidores);

            break;
        case 2:
            informarRepartidores(matrizRepartidores, 14, 80);
            break;
        case 3:
            informarTransportesNoDisponibles(matrizRepartidores, 14, 80);
            break;
        case 4:
            informarZonasConMasRepartidores(matrizRepartidores, 14, 80);
            break;
        case 5:
            ingresarPedidos(primerNodoPedido, matrizRepartidores);
            break;
        case 6:
            asignarPedidos(matrizRepartidores, primerNodoPedido, primerNodoRepartidor);
            break;
        case 7:
            cout<<"--------PEDIDOS ENTREGADOS-------"<<endl<<endl;
            mostrarPedidosEntregados(matrizRepartidores, primerNodoRepartidor);
            break;
        case 9:
            crearArbolComercios(primerNodoRepartidor);
            escribirArchivoConPedidosNoEntregados(primerNodoPedido);
            return;
        default:
            cout << "Ingreso incorrecto";
            break;
        }

        if(opcion != 1 && opcion != 5 && opcion != 6){ // Estas son las opciones que ya tienen metodo para cerrarse
            string in;
            cout << endl << "Ingrese CUALQUIER TECLA para volver: ";
            cin >> in; // Detiene el sistema para que el usuario pueda ver la funcionalidad que selecciono
        }

        clearTerminal();
        
        
    }
}