

#include <iostream>

using namespace std;

#include"../include/utils.h"
#include"../include/Pedido.h"
#include"../include/Repartidor.h"

void mostrarPedidosEntregados(Repartidor matrizRepartidores[][80], NodoListaRepartidor*& primerRepartidor){
    NodoListaRepartidor* repActual = primerRepartidor;

    if(repActual == NULL){
        cout<<"Todavia no hay pedidos entregados"<<endl;
        return;
    }

    while (repActual != NULL) {
        
        Repartidor repartidor = returnRepartidorByDni(matrizRepartidores, repActual->dniRepartidor);
        cout << "Pedidos de " << repartidor.nombre << " " << repartidor.apellido<< ", DNI: "<<repartidor.dni << ", Vehiculo: "<<repartidor.transporte<<"."<<endl<<endl;
        
        Pedido pedido;
        NodoListaPedido* pedidoActual = repActual->primerNodoPedido;
        while (pedidoActual != NULL) {
            pedido = pedidoActual->pedido;
            cout<<"Zona: "<<pedido.zona<<endl;
            cout<<"Importe: "<<pedido.importe<<endl;
            cout<<"Volumen m3: "<<pedido.volumen<< "  ---> Vehiculo necesario: "<< returnTransporteByVolumenPedido(pedido.volumen) <<endl;
            cout<<"Domicilio: "<<pedido.domicilio<<endl;
            cout<<"Codigo del comercio: "<<pedido.codigoComercio<<endl;
            cout<<"------------------------"<<endl;

            pedidoActual = pedidoActual->proxNodoPedido;
        }

        cout << endl << endl;

        repActual = repActual->proxNodoRepartidor;
    }
}