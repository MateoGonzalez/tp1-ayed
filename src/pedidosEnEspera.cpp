
#include "../include/Pedido.h"
#include "../include/pedidosEnEspera.h"
#include "../include/utils.h"

#include<iostream>
#include<string>
using namespace std;



void ingresarPedidos(NodoListaPedido *& primerNodoPedido, Repartidor matrizRepartidores[][80])
{
    
    cin.ignore();

    cout<< "--------INGRESAR PEDIDO---------";

    Pedido pedido;

    do
    {
        cout<<endl<<endl;
        cout << "Zona (0 para salir): ";
        string zonaStr;
        getline(cin, zonaStr);

        if (zonaStr == "0") {
            break;
        }

        if (!isNumber(zonaStr) || zonaStr.empty()) {
            cout << "Zona debe ser un número del 1-15. Vuelve a ingresar.";
            continue; 
        }
        pedido.zona = stoi(zonaStr);

        if(pedido.zona > 15 || pedido.zona < 1){
            cout << "Zona debe ser un número del 1-15. Vuelve a ingresar.";
            continue; 
        }
        

        do
        {
            cout << "Domicilio: ";
            string domicilio;
            getline(cin, domicilio);

            if (domicilio.empty()) {
                cout << "Debe ingresar un domicilio." << endl<<endl;
                continue; 
            }

            pedido.domicilio = domicilio;

            break; 
        } while (true);

        do
        {
            cout << "Volumen: ";
            string volumenStr;
            getline(cin, volumenStr);

            if (!isNumOrFloat(volumenStr) || volumenStr.empty()) {
                cout << "Volumen debe ser un número válido. Vuelve a ingresar." << endl;
                continue; 
            }

            pedido.volumen = stof(volumenStr);
            break; 
        } while (true);

        do
        {
            cout << "Importe: ";
            string importeStr;
            getline(cin, importeStr);

            if (!isNumOrFloat(importeStr) || importeStr.empty()) {
                cout << "Importe debe ser un número válido. Vuelve a ingresar." << endl;
                continue; 
            }

            pedido.importe = stof(importeStr);
            break; 
        } while (true);

        do
        {
            cout << "Codigo del comercio: ";
            string codigoStr;
            getline(cin, codigoStr);

            if (!isNumber(codigoStr) || codigoStr.empty()) {
                cout << "Codigo de comercio debe ser un número entero valido. Vuelve a ingresar." << endl;
                continue; 
            }

            pedido.codigoComercio = stoi(codigoStr);
            break; 
        } while (true);


        string transporte = returnTransporteByVolumenPedido(pedido.volumen);

        bool existeRepartidorDisponible = returnExisteRepartidorConZonaYTransporte(matrizRepartidores, pedido.zona, transporte);

        if(existeRepartidorDisponible){
            agregarPedidoAListaDeEspera(primerNodoPedido, pedido);
            
            cout<<endl<<"Pedido ingresado!"<<endl;
        }else{
            cout<<endl<<"No hay un repartidor con "<< transporte << " disponible en la zona "<< pedido.zona <<endl<<endl;
            cout<<"Ingrese otro:";
        }
        

    } while (true);

   
}




void agregarPedidoAListaDeEspera(NodoListaPedido*& primerNodoPedido, Pedido pedido) {
    NodoListaPedido* nuevoNodo = new NodoListaPedido;
    nuevoNodo->pedido = pedido;
    nuevoNodo->proxNodoPedido = NULL;

    if (primerNodoPedido == NULL) {
        primerNodoPedido = nuevoNodo;
    } else {
        NodoListaPedido* nodoActual = primerNodoPedido;
        while (nodoActual->proxNodoPedido != NULL) {
            nodoActual = nodoActual->proxNodoPedido;
        }
        nodoActual->proxNodoPedido = nuevoNodo;
    }
}

Pedido sacarPedidoPorZonaYTransporte(NodoListaPedido*& primerNodoPedido, int zona, string transporte) {
    NodoListaPedido* nodoActual = primerNodoPedido;
    NodoListaPedido* nodoAnterior = NULL;

    Pedido pedidoActual;

    while (nodoActual != NULL) {
        pedidoActual = nodoActual->pedido;

        if (returnTransporteByVolumenPedido(pedidoActual.volumen) == transporte && pedidoActual.zona == zona) {
            if (nodoAnterior == NULL) {
               
                primerNodoPedido = nodoActual->proxNodoPedido;
            } else {
                nodoAnterior->proxNodoPedido = nodoActual->proxNodoPedido;
            }
            delete nodoActual;
            return pedidoActual;
        }

        nodoAnterior = nodoActual;
        nodoActual = nodoActual->proxNodoPedido;
    }

   
    return Pedido(); 
}



Pedido sacarPrimerPedidoEnEspera(NodoListaPedido*& primerNodoPedido) {
    if (primerNodoPedido != nullptr) {
        NodoListaPedido* nodoAEliminar = primerNodoPedido;
        primerNodoPedido = primerNodoPedido->proxNodoPedido;
        Pedido pedido = nodoAEliminar->pedido;
        delete nodoAEliminar;
        return pedido;
    } else {
        return Pedido();
    }
}
