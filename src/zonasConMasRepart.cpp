using namespace std;

#include "../include/zonasConMasRepart.h"

void informarZonasConMasRepartidores(Repartidor matriz[][80], int filas, int colum){

    int cantMayor = 0;

    for(int i = 0; i < colum;i++){
        if(matriz[0][i].dni == 0)
            break;
        
        cantMayor++;
    }

    for(int i = 1; i < filas; i++){
        int contador = 0;
        for(int j = 0; j < colum; j++){
            if(matriz[i][j].dni == 0)
                break;
            
        contador++;
        } 
        if(contador > cantMayor) cantMayor = contador;
        
    }

    if(cantMayor == 0)
        cout<<"No existe ningun repartidor en ninguna zona"<<endl;
        
    else{
        cout << "La o las zonas con mayor numero de repartidores son:" << endl << endl;

        for(int i = 0; i < filas; i++){
            int contador = 0;
            for(int j = 0; j < colum; j++){
                if(matriz[i][j].dni == 0){
                    break;
                }
            contador++;
            } 
            if(contador == cantMayor){
                cout << "Zona "<< i + 1 << endl << endl;
            }
        }
    }
}