#include<iostream>
using namespace std;

#include "../include/Repartidor.h"

void informarRepartidores(Repartidor matrizRepartidores[][80], int filas, int colum){
    int matrizMuestra[14][4];
    cout<<"Vehic.     MOTO   AUTO  CAMION  CAMIONETA"<<endl;

    for(int i=0; i<14;i++){

        int cantMoto = 0, cantAuto = 0, cantCamioneta = 0, cantCamion = 0;
         
        for(int j=0;j<80;j++){

            if(matrizRepartidores[i][j].transporte == "moto") cantMoto += 1;
            else if(matrizRepartidores[i][j].transporte == "auto") cantAuto += 1;
            else if(matrizRepartidores[i][j].transporte == "camioneta") cantCamioneta += 1;
            else if(matrizRepartidores[i][j].transporte == "camion") cantCamion += 1;

        }

        matrizMuestra[i][0] = cantMoto;
        matrizMuestra[i][1] = cantAuto;
        matrizMuestra[i][2] = cantCamion;
        matrizMuestra[i][3] = cantCamioneta;
         
        if(i > 8) cout<<"Zona "<<i+1;
        else cout<<"Zona "<<i+1<<" ";

        for(int j=0;j<4;j++){  
            if(j == 3) 
                cout<<"  ";
            int num = matrizMuestra[i][j];
            cout<<"     ";

            if(num>9) 
                cout<<num;
            else 
                cout<<" "<<num;
        }
        cout<<endl;
    }
}
