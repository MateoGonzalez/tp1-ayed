#include <iostream>

using namespace std;

#include "../include/Pedido.h"
#include "../include/pedidosEnEspera.h"
#include "../include/pedidosNoEntregados.h"


void escribirArchivoConPedidosNoEntregados(NodoListaPedido*& primerNodoPedido){
    
    Pedido pedido;
    int count = 0;

    cout<<endl<<"---------PEDIDOS SIN ENTREGAR---------"<<endl<<endl;
    
    FILE *archivoPedidosNoEntregados = fopen("ArchivosGenerados/PedidosNoEntregados.dat", "wb");

    while(primerNodoPedido != NULL){
        count+=1;
        pedido = sacarPrimerPedidoEnEspera(primerNodoPedido);
        fwrite(&pedido, sizeof(pedido), 1, archivoPedidosNoEntregados);
    }

    if(count > 0){
        cout<<"Se escribieron en el archivo los " << count<< " pedidos sin entregar!"<<endl<<endl;
    }else{
        cout<<"No se escribio el archivo de PedidosNoEntregados.dat ya a que no quedaron pedidos en espera."<<endl<<endl;
    }
}

