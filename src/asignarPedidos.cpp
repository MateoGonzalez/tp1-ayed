#include<iostream>
#include "../include/asignarPedidos.h"
#include "../include/utils.h"
#include "../include/pedidosEnEspera.h"
#include "../include/Pedido.h"

using namespace std;

void asignarPedidos(Repartidor matrizRepartidores[][80], NodoListaPedido*& primerNodoPedido, NodoListaRepartidor*& nodoPrimerRepartidor){

    cout<<"--------ASIGNAR PEDIDOS--------"<<endl<<endl;

    if(nodoPrimerRepartidor == NULL){
        cout<<"No hay pedidos en espera."<<endl;
        string in;
        cout << endl << "Ingrese CUALQUIER TECLA para volver: ";
        cin>>in;
        return;
    }

    string input;
    cin.ignore();
    do{
        cout<<"Ingrese el DNI del repartidor (0 para salir): ";
        getline(cin, input);

        if(input == "0") break;
        
        if(!isNumber(input) || input.empty()) {
            cout<<"El DNI debe ser un numero sin puntos"<<endl<<endl;
            continue;
        }
        unsigned dni = stoul(input);

        bool existe = returnRepartidorExisteDni(matrizRepartidores, 14, 80, dni);

        if(!existe){
            cout<<"No existe un repartidor con ese DNI"<<endl<<endl;
            continue;
        }

        Repartidor repartidor = returnRepartidorByDni(matrizRepartidores, dni);

        int zonaRepartidor = returnZonaRepartidorByDni(matrizRepartidores, dni);

        Pedido pedido = sacarPedidoPorZonaYTransporte(primerNodoPedido, zonaRepartidor, repartidor.transporte);

        if(pedido.zona == 0){
            cout<<"No hay pedidos para este repartidor"<<endl<<endl;
            continue;
        }

        NodoListaRepartidor* nodoRepartidor = returnRepartidorYCrearSiNoExiste(nodoPrimerRepartidor, dni);

        agregarPedidoARepartidor(nodoRepartidor, pedido);


        cout<<endl<<"Se agrego un pedido al repartidor con DNI "<<dni<<"!"<<endl<<endl;

    }while(input != "0");   
}


NodoListaRepartidor* returnRepartidorYCrearSiNoExiste(NodoListaRepartidor*& primerRepartidor, unsigned dniRepartidor){


    NodoListaRepartidor* repartidorActual = primerRepartidor;

    NodoListaRepartidor* nuevoRepartidor = new NodoListaRepartidor;

    bool existe = false;

    while (repartidorActual != NULL){
        if(repartidorActual->dniRepartidor == dniRepartidor){
            existe = true;
            nuevoRepartidor = repartidorActual;
            break;
        }
        repartidorActual = repartidorActual->proxNodoRepartidor;
    }
    
   
    if(!existe){
        NodoListaPedido* primerNodoPedido = NULL;

        nuevoRepartidor->dniRepartidor = dniRepartidor;
        nuevoRepartidor->primerNodoPedido = primerNodoPedido;
        nuevoRepartidor->proxNodoRepartidor = NULL;  

        if (primerRepartidor == NULL || dniRepartidor < primerRepartidor->dniRepartidor) {
            nuevoRepartidor->proxNodoRepartidor = primerRepartidor;
            primerRepartidor = nuevoRepartidor;
            return nuevoRepartidor;
        }

        NodoListaRepartidor* actual = primerRepartidor;
        while (actual->proxNodoRepartidor != NULL && actual->proxNodoRepartidor->dniRepartidor < dniRepartidor) {
            actual = actual->proxNodoRepartidor;
        }
        
        nuevoRepartidor->proxNodoRepartidor = actual->proxNodoRepartidor;
        actual->proxNodoRepartidor = nuevoRepartidor;
    }

    return nuevoRepartidor;
}

void agregarPedidoARepartidor(NodoListaRepartidor *& nodoRepartidor, Pedido pedido) {

    NodoListaPedido* nuevoNodoPedido = new NodoListaPedido;
    nuevoNodoPedido->pedido = pedido;
    nuevoNodoPedido->proxNodoPedido = NULL;

    if (nodoRepartidor->primerNodoPedido == NULL || pedido.importe > nodoRepartidor->primerNodoPedido->pedido.importe) {
        nuevoNodoPedido->proxNodoPedido = nodoRepartidor->primerNodoPedido;
        nodoRepartidor->primerNodoPedido = nuevoNodoPedido;
    } else {
        NodoListaPedido* actual = nodoRepartidor->primerNodoPedido;
        while (actual->proxNodoPedido != NULL && pedido.importe < actual->proxNodoPedido->pedido.importe) {
            actual = actual->proxNodoPedido;
        }
        nuevoNodoPedido->proxNodoPedido = actual->proxNodoPedido;
        actual->proxNodoPedido = nuevoNodoPedido;
    } 

}

