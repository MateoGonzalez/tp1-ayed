#include <iostream>
#include <cstring>

using namespace std;

char *concat(const char *a, const char *b)
{
    int lena = strlen(a);
    int lenb = strlen(b);
    char *con = reinterpret_cast<char *>(malloc(lena + lenb + 1));
    memcpy(con, a, lena);
    memcpy(con + lena, b, lenb + 1);
    return con;
}

FILE *abrirArchivos(const char *modo, string transporte)
{
    transporte[0] = toupper(transporte[0]);
    string transporteFile = "Rep" + transporte + ".dat";
    const char *fileLocation = "ArchivosGenerados/";

    FILE *f = fopen(concat(fileLocation, &transporteFile[0]), modo);

    if (!f)
        return NULL;

    return f;
}