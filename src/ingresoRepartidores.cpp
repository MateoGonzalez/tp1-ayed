#include <iostream>

#include "../include/ingresoRepartidores.h"
#include "../include/utils.h"
#include "../include/escribirArchivos.h"
#include <limits>

using namespace std;


void ingresoRepartidores(Repartidor matrizRepartidores[][80])
{
    
    cin.ignore();

    cout<< "--------INGRESAR PEDIDO---------";

    Repartidor repartidor;
    int zona = 0;

    do{
        cout<<endl<<endl;
        cout << "Zona (0 para salir): ";
        string zonaStr;
        getline(cin, zonaStr);

        if (zonaStr == "0") {
            break; // Salir si la zona es 0
        }

        if (!isNumber(zonaStr) || zonaStr.empty()) {
            cout << "Zona debe ser un número del 1-15. Vuelve a ingresar.";
            continue; 
        }
        zona = stoi(zonaStr);

        if(zona > 15 || zona < 1){
            cout << "Zona debe ser un número del 1-15. Vuelve a ingresar.";
            continue; 
        }
    
    
        do{
            string transporte;    
            cout << "Transporte (moto, auto, camioneta, camion): ";
        
            getline(cin, transporte);

            if (transporte.empty()) {
                cout << "Debe ingresar un transporte." << endl;
                continue; 
            }
            if(transporte != "moto" && transporte != "auto" && transporte != "camioneta" && transporte != "camion"){
                cout<< "Debe ingresar un transporte valido (moto, auto, camioneta, camion)."<<endl;
                continue;
            }

            repartidor.transporte = transporte;
            break; 

        } while (true);

        do{
            cout << "Nombre: ";
            string nombre;
            getline(cin, nombre);

            if (nombre.empty()) {
                cout << "Ingrese un nombre." << endl;
                continue; 
            }
            if(isNumber(nombre)) {
                cout<< "El nombre no puede ser un numero."<<endl;
                continue;
            }
            repartidor.nombre = nombre;
            break; 
        } while (true);

        do{
            cout << "Apellido: ";
            string apellido;
            getline(cin, apellido);

            if (apellido.empty()) {
                cout << "Debe ingresar un apellido." << endl;
                continue; 
            }
            if(isNumber(apellido)) {
                cout<< "El apellido no puede ser un numero."<<endl;
                continue;
            }

            repartidor.apellido = apellido;

            break; 
        } while (true);

        do{
            cout << "DNI: ";
            string dniStr;
            getline(cin, dniStr);

            if (dniStr.empty() || !isNumber(dniStr)) {
                cout << "Debe ingresar un DNI sin puntos." << endl<<endl;
                continue; 
            }

            repartidor.dni = stoi(dniStr);
            
            if(repartidor.dni <= 1000000 || repartidor.dni > 99999999){
                cout << "El DNI que ingreso es invalido. (1000000 - 99999999) " << endl<<endl;
                continue;
            }

            if(returnRepartidorExisteDni(matrizRepartidores, 14, 80, repartidor.dni)){
                cout<< "Ya existe un repartidor con ese DNI."<<endl<<endl;
                continue;
            }

            break; 
        } while (true);

        do{
            string patente;    
            cout << "Patente: ";
        
            getline(cin, patente);

            if (patente.empty()) {
                cout << "Debe ingresar un patente." << endl;
                continue; 
            }
            if(!validarFormatoPatente(patente)){
                cout<<endl<<"La patente que ingreso es invalida! Formatos: ABC-123 / AB-123-CD"<<endl<<endl;
                continue;
            }

            repartidor.patente = patente;
            break; 

        } while (true);

        agregarRepartidorMatriz(matrizRepartidores, repartidor, 80, zona);
        
        escribirArchivos(matrizRepartidores, zona, repartidor.transporte);

        clearTerminal();

        cout<<endl<<"REPARTIDOR AGREGADO!"<<endl << endl;

        cout<<"Zona: "<<zona<<endl;
        cout<<"Vehiculo: "<<repartidor.transporte<<endl;
        cout<<"DNI: "<<repartidor.dni<<endl;
        cout<<"Nombre: "<<repartidor.nombre<<endl;
        cout<<"Apellido: "<<repartidor.apellido<<endl;
        cout<<"Patente: "<<repartidor.patente<<endl<<endl;
        
        cout<<"AGREGAR OTRO REPARTIDOR: "<<endl<<endl;
        

    } while (true);
   
}
