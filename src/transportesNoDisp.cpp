#include <iostream>

using namespace std;

#include "../include/Repartidor.h"

void informarTransportesNoDisponibles(Repartidor matrizRepartidores[][80], int filas, int colum){
    bool autos=true, camiones=true, motos=true, camionetas=true;

    for (int i = 0; i < filas; i++)
    {
        for (int j = 0; j < colum; j++)
        {
            if(matrizRepartidores[i][j].transporte=="auto"){
                autos=false;
            }
            else if(matrizRepartidores[i][j].transporte=="camion"){
                camiones=false;
            }
            else if(matrizRepartidores[i][j].transporte=="moto"){
                motos=false;
            }
            else if(matrizRepartidores[i][j].transporte=="camioneta"){
                camionetas=false;
            }
        }
    }
    
    if(!autos && !camiones && !motos && !camionetas){
        cout<<endl<<"Todos los transportes estan disponibles."<<endl;
        return;
    }
        

    cout<<"Transportes no disponibles:"<<endl;
    if(autos) cout<<"Autos"<<endl;
    if(camiones) cout<<"Camiones"<<endl;
    if(motos) cout<<"Motos"<<endl;
    if(camionetas) cout<<"Camionetas"<<endl;
}