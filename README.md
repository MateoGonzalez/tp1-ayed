# TP1-AyED

## INSTRUCTIVO

Comando para compilar ```g++ main.cpp src/* utils/* -o main```  

Luego ejecutar el archivo generado

### Pantalla

La funcion pantalla muestra en consola una lista:

1 - Ingresar repartidor  
2 - Informar repartidores por zona  
3 - Informar transporte no disponible  
4 - Informar zona con mas repartidores  
5 - Ingresar pedidos  
6 - Asignar pedido a un repartidor  
7 - Mostrar repartidores que hicieron entregas  
9 - Cerrar   

El usuario debe ingresar por consola el numero de la funcionalidad que desea utilizar.  
El sistema le permite ir y volver de funcion en funcion, es decir que puede agregar un repartidor,
luego ir a ver la cantidad de repartidores por zona o cualquiera de las otras y luego volver a agregar otro repartidor las veces que quiera.  


## VARIABLES

```c++
struct Repartidor{
    string nombre;
    string apellido;
    unsigned dni;
    string transporte;
}

struct Pedido {
    string domicilio;
    int zona;
    float volumen;
    float importe;
    int codigoComercio;
};

struct NodoListaPedido {
    Pedido pedido;
    NodoListaPedido* proxNodoPedido;
};

struct NodoListaRepartidor{
    int dniRepartidor;
    NodoListaPedido* primerNodoPedido;

    NodoListaRepartidor* proxNodoRepartidor;
};

NodoListaPedido* primerNodoPedido
NodoListaRepartidor *primerNodoRepartidor = NULL;
```



Elegimos guardar el nombre, apellido, dni y transporte dentro del objeto repartidor y dejar el numero de fila de la matriz como numero de zona.

Matriz de repartidores:

```c++
Repartidor matrizRepartidores[14][80];    
```

La matriz de repartidores cuenta con 14 filas (1 por cada zona)
Cada fila corresponde a un array de repartidores de dicha zona

matrizRepartidores[0][] array de repartidores de la zona 1  
matrizRepartidores[1][] array de repartidores de la zona 2  
matrizRepartidores[2][] array de repartidores de la zona 3  
y asi ...

Cuenta con hasta 80 columnas, dado a que pueden existir hasta 20 repartidores por vehiculo por zona.  
  
Es decir, en todas las zonas pueden existir hasta 20 autos, 20 camiones, 20 motos y 20 camionetas.  


## Pedidos  

Elegimos guardar los pedidos en espera en una lista simple, ya que no buscamos acceder ni al primero, ni al ultimo, accedemos al primero disponible segun las caracteristicas del repartidor (su vehiculo y su zona).  

El ```NodoListaRepartidor *primerNodoRepartidor = NULL;``` guarda en forma de lista, los repartidores y por cada repartidor, los pedidos que entrego.  

### 1- Ingresar repartidor

Se le pedira al usuario que ingrese los datos del repartidor
que desea ingresar y debe ingresar 0 cuando desee finalizar

Al finalizar se guardaran los repartidores en los archivos .dat

Una vez que termino de ingresar podra volver a la pantalla principal 
en la que podra elegir que informacion desea ver.  

El programa no permite el ingreso de codigo de zona o nombre de transporte incorrecto, se le volvera a pedir en caso de que asi sea.  
Tambien se tiene en cuenta la posibilidad de que el usuario intente agregar un repartidor con un transporte a una zona que ya no cuenta con mas espacio para ese transporte (hasta 20). En ese caso se le informara al usuario que no hay mas lugar en esa zona para ese transporte y debera ingresar los datos de nuevo.  
 
### 2 - Informar repartidores por zona  

Le muestra al usuario una matriz visual, en la que las filas son las zonas y las columnas son los transportes. Por cada uno de ellos se muestra la cantidad de
repartidores que existen.  

### 3 - Informar transporte no disponible  

Le muestra al usuario los transportes no disponibles (que no utiliza ningun repartidor).  

### 4 - Informar zona con mas repartidores  

Informa la o las zonas con mas repartidores.  

### 5- Ingresar pedidos  

Le permite al usuario agregar un pedido a la lista de espera.  
El programa primero verifica si existe un repartidor con esa zona y el transporte necesario en la matriz.  
Si no hay un repartidor capaz de entregar ese pedido (por su volumen) se le avisara al usuario.  
En caso contrario, el pedido sera agregado.  

El programa agrega el pedido al final de la lista de pedidos.  

### 6- Asignar pedido a un repartidor  

El usuario debe ingresar el DNI del repartidor que deba realizar una entrega.  
Dado el DNI, se accede a los detalles del repartidor, y luego se verifica si existe algun pedido en espera que el repartidor pueda entregar.  
Si se cumple esa condicion, el pedido se saca de la lista de espera.  
Luego se busca el nodo del repartidor (si no existe se crea) en la lista de pedidos entregados, y se agrega el pedido en la lista de pedidos de ese repartidor.  

### 7- Mostrar repartidores que hicieron entregas  

El usuario puede ver los repartidores que hicieron entregas, ordenados por DNI (de menor a mayor) y por cada uno de ellos los pedidos que entregaron, ordenados por el importe de mayor a menor.  

### 9 - Cerrar  

Al cerrar el programa, se genera el arbol que ordena los codigos de comercio con el metodo INORDER y su respectiva cantidad de pedidos.  
Ademas se le muestra al usuario la cantidad de pedidos sin entregar que fueron agregados al archivo de ```PedidosNoEntregados.dat```.  

## MODULARIZACION  

Elegimos modularizar el proyecto ya que el sistema cuenta con multiples funcionalidades y tenerlo todo en un mismo archivo puede llevar a la confusion o la dificultad para leer el codigo (y mas en un trabajo de equipo).  
Tambien elegimos utilizar git y para esto la modularizacion es importante, cada uno genera una rama y el archivo de la funcionalidad que hay que agregar y asi se evitan conflictos.  

Esta es la disposicion de las carpetas y archivos en el proyecto:  

```c++
    main.cpp
    include
    |------ utils.h
    		funcion1.h
    		funcion2.h
            ...
    src
    |------ funcion1.cpp
    		funcion2.cpp
            ...
    utils
    |------ utils.cpp

    ArchivosGenerados
    |------ RepAuto.dat
            RepMoto.dat
            RepCamioneta.dat
            RepCamion.dat
```  

En la carpeta src/ ("source") se guarda todo el codigo fuente o archivos .cpp que tienen las funciones necesarias para el sistema.  
include/ cuenta con los archivos "header" que son las inicializaciones de las funciones o variables que se utilizan en el proyecto y son necesarias en otros archivos.  
En la carpeta utils/ se guardan todas las funciones utiles que no corresponden a una funcion en concreto, por ejemplo la inicializacion de una matriz o el ordenamiento de una matriz.   


## FUNCIONES

### Escribir archivos

Cada vez que el usuario ingresa un repartidor, el mismo se agrega a la matrizRepartidores en la primer posicion donde el dni sea 0 (esto significa que los repartidores existentes siempre estaran al principio y los repartidores "nulos" se econtraran al final).  

Luego se le pasa a la funcion de escribirArchivo los siguientes parametros:  

```c++
void escribirArchivo(Repartidor matrizRepartidores[][80], int zona, string transporte)
```  

Esta funcion inicializa un vector de repartidores:  

```c++
RepartidorArchivo vectorRepartidoresPorTransporte[280];
```  

en el que se van a guardar todos los repartidores que existan con el transporte que se paso por parametro.
Esto lo hacemos para luego ordenar el vector de repartidores con el mismo transporte y reescribir el archivo con los repartidores ordenados.  
El vector tiene 280 posiciones ya que si tenemos en cuenta de que pueden existir hasta 20 repartidores con el mismo transporte por zona 14 x 20 = 280, esto quiere decir que pueden existir hasta 280 repartidores por transporte en todo el sistema.  
<br></br>
Utilizamos un vector para que solo actualice el transporte.dat del repartidor que se acaba de agregar y no reescriba todos los archivos innecesariamente.  

Por ejemplo:  

Al agregar un repartidor con moto en la zona 10, cuando entre a esta funcion va a llenar el vector con todos los repartidores con moto del sistema, los va a ordenar por DNI y luego va a escribir en el archivo RepMoto.dat el objeto:

```c++
struct RepartidorArchivo{
    int zona;
    unsigned dni;
    string nombre;
    string apellido;
    string patente;
};
```  



### 2- Informar repartidores por zona
Genera una matriz visual en consola que muestra la cantidad de repartidores
por vehiculo y por zona.

```c++
void informarRepartidores(Repartidor matrizRepartidores[][80], int filas, int colum);
```  


### 3- Informar que medios de transportes no hay disponibles 
Es decir, no existe ningun repartidor en ninguna zona con esos transportes
Imprime en pantalla cada uno de esos transportes.

```c++
void informarTransportesNoDisponibles(Repartidor matrizRepartidores[][80], int filas, int colum);
```

### 4- Informar la o las zonas en las que hay mas repartidores inscriptos (sin importar el transporte)
Si existe solo una zona con mayor cantidad imprime esa zona, 
si existe mas de una con misma mayor cantidad, imprime todas

```c++
void informarZonasConMasRepartidores(Repartidor matriz[][80], int filas, int colum);
```

### 5- Ingresar pedidos  

```c++
void ingresarPedidos(NodoListaPedido *& , Repartidor [][80]);
```  

### 6- Asignar pedido a un repartidor  

```c++
void asignarPedidos(Repartidor [][80], NodoListaPedido*&, NodoListaRepartidor*&);
```  

### 7- Mostrar repartidores que hicieron entregas  

```c++
void mostrarPedidosEntregados(Repartidor [][80], NodoListaRepartidor*&);
```   

### 9 - Cerrar 

```c++
void crearArbolComercios(NodoListaRepartidor*& primerNodoRepartidor);  
void escribirArchivoConPedidosNoEntregados(NodoListaPedido*& primerNodoPedido);
```  

<!-- <br></br>

## IMAGENES  

### MENU

<br></br>
<br></br>
<br></br>
<br></br>

### 1 - Ingresar repartidor

<br></br>
<br></br>
<br></br>
<br></br>

### 2 - Informar repartidores por zona  

<br></br>
<br></br>
<br></br>
<br></br>
<br></br>

### 3 - Informar transporte no disponible

<br></br>
<br></br>
<br></br>
<br></br>
<br></br>

### 4 - Informar zona con mas repartidores  

<br></br>
<br></br>
<br></br>
<br></br>
<br></br>
 -->
