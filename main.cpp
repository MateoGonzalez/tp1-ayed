#include<iostream>
using namespace std;
#include "include/pantalla.h"
#include <cstdlib>

#include "include/Repartidor.h"
#include "include/informarRepartidores.h"
#include "include/escribirArchivos.h"
#include "include/Repartidor.h"
#include "include/leerArchivos.h"
#include "include/utils.h"
#include "include/Pedido.h"
#include "include/crearDatos.h"
#include "include/arbolComercios.h"

bool returnExisteArchivo(const char* pathArchivo){
    FILE* archivo = fopen(pathArchivo, "r");
    bool existe = archivo != NULL;
    if (archivo) {
        fclose(archivo);
    }
    return existe;
}

void crearArchivos(){
    if(!returnExisteArchivo("ArchivosGenerados/RepAuto.dat")) fopen("ArchivosGenerados/RepAuto.dat", "wb");
    if(!returnExisteArchivo("ArchivosGenerados/RepMoto.dat")) fopen("ArchivosGenerados/RepMoto.dat", "wb");
    if(!returnExisteArchivo("ArchivosGenerados/RepCamioneta.dat")) fopen("ArchivosGenerados/RepCamioneta.dat", "wb");
    if(!returnExisteArchivo("ArchivosGenerados/RepCamion.dat")) fopen("ArchivosGenerados/RepCamion.dat", "wb");
    if(!returnExisteArchivo("ArchivosGenerados/PedidosNoEntregados.dat")) fopen("ArchivosGenerados/PedidosNoEntregados.dat", "wb");
}

int main(){
    crearArchivos();

    NodoListaPedido *primerNodoPedido = NULL;

    NodoListaRepartidor *primerNodoRepartidor = NULL;

    Repartidor matrizRepartidores[14][80];

    inicializarMatrizRepartidores(matrizRepartidores, 14, 80);
    leerArchivos(matrizRepartidores);

    // development: LLena la matriz y los Nodos con repartidores random
    // Agrega 8 pedidos y asigna 5 a 3 repartidores
    //---------------------------------------------------------------------------------------
    crearDatos(matrizRepartidores, primerNodoPedido, primerNodoRepartidor);
    //---------------------------------------------------------------------------------------

    pantalla(matrizRepartidores, primerNodoPedido, primerNodoRepartidor);

    cout<<endl;
    return 0;

}
