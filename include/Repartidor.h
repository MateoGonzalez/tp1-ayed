#ifndef REPARTIDOR_H
#define REPARTIDOR_H

#include<iostream>

using namespace std;

struct Repartidor{
    unsigned dni;
    string nombre;
    string apellido;
    string patente;
    string transporte;
};

#endif