#ifndef CREAR_DATOS_H
#define CREAR_DATOS_H

#include"Repartidor.h"
#include"Pedido.h"
using namespace std;

#include "escribirArchivos.h"
#include "asignarPedidos.h"
#include "pedidosEnEspera.h"

void agregarRepartidores(Repartidor matrizRepartidores[][80]){
    // Zona 1
    matrizRepartidores[0][0] = {12313123, "Matias", "Rodriguez", "abc-123", "auto"};
    matrizRepartidores[0][1] = {21321312, "Luis", "Perez", "xyz-789", "moto"};
    matrizRepartidores[0][2] = {12133322, "Ana", "Rodriguez", "def-456", "auto"};
    matrizRepartidores[0][3] = {44543545, "Carlos", "Lopez", "ghi-987", "camioneta"};

    // Zona 2
    matrizRepartidores[1][0] = {56546555, "María", "Martinez", "jkl-654", "auto"};
    matrizRepartidores[1][1] = {77676664, "David", "Gomez", "mno-321", "moto"};
    matrizRepartidores[1][2] = {44445645, "Elena", "Hernandez", "pqr-852", "camion"};
    matrizRepartidores[1][3] = {76763232, "Juan", "Sanchez", "stu-963", "camioneta"};

    // Zona 3
    matrizRepartidores[2][0] = {43324432, "Laura", "Fernandez", "vwx-147", "auto"};
    matrizRepartidores[2][1] = {23242123, "Pedro", "Ramirez", "yz1-369", "moto"};
    matrizRepartidores[2][2] = {65544446, "Sofia", "Garcia", "abc-789", "camion"};
    matrizRepartidores[2][3] = {54656353, "Andres", "Torres", "def-258", "camioneta"};

    // Zona 4 
    matrizRepartidores[3][0] = {23423211, "Luisa", "Vargas", "ghi-741", "auto"};
    matrizRepartidores[3][1] = {23423222, "Pablo", "Ortega", "jkl-963", "moto"};
    matrizRepartidores[3][2] = {656556565, "Julia", "Rojas", "mno-852", "camion"};
    matrizRepartidores[3][3] = {89089089, "Javier", "Diaz", "pqr-369", "camioneta"};

    // Zona 5
    matrizRepartidores[4][0] = {12312312, "Isabel", "Luna", "stu-741", "auto"};
    matrizRepartidores[4][1] = {23423423, "Hugo", "Paredes", "vwx-258", "moto"};
    matrizRepartidores[4][2] = {34534534, "Natalia", "Soto", "yz1-963", "camion"};
    matrizRepartidores[4][3] = {45645645, "Roberto", "Rios", "abc-741", "camioneta"};

    // Zona 6
    matrizRepartidores[5][0] = {56756756, "Veronica", "Mendoza", "def-369", "auto"};
    matrizRepartidores[5][1] = {67867867, "Ricardo", "Cabrera", "ghi-852", "moto"};

    escribirArchivos(matrizRepartidores, 1, "moto");
    escribirArchivos(matrizRepartidores, 1, "auto");
    escribirArchivos(matrizRepartidores, 1, "camion");
    escribirArchivos(matrizRepartidores, 1, "camioneta");
}

void crearDatos(Repartidor matrizRepartidores[][80], NodoListaPedido*& primerNodoPedido, NodoListaRepartidor*& primerNodoRepartidor) {
    
    agregarRepartidores(matrizRepartidores);

    //Zona 1, Auto, DNI: 12313123
    Pedido ped1 = {"constituyentes 2300", 1, 0.01, 2200, 1};
    Pedido ped2 = {"cullen 1300", 1, 0.01, 5600, 2};
    Pedido ped3 = {"Mozart 3100", 1, 0.01, 3300, 2};

    //Zona 5, Camion, DNI: 34534534
    Pedido ped4 = {"Libertador 3010", 5, 10, 12321, 10};
    Pedido ped5 = {"Gral Paz", 5, 10, 54544, 10};


    //Zona3, Moto, DNI: 23242123
    Pedido ped6 = {"Larralde 3400", 3, 0.001, 3500, 10};
    Pedido ped7 = {"Ezeiza 2500", 3, 0.001, 120, 10};
    Pedido ped8 = {"Lugones 1222", 3, 0.001, 2400, 2};

    agregarPedidoAListaDeEspera(primerNodoPedido, ped1);
    agregarPedidoAListaDeEspera(primerNodoPedido, ped2);
    agregarPedidoAListaDeEspera(primerNodoPedido, ped3);
    agregarPedidoAListaDeEspera(primerNodoPedido, ped4);
    agregarPedidoAListaDeEspera(primerNodoPedido, ped5);
    agregarPedidoAListaDeEspera(primerNodoPedido, ped6);
    agregarPedidoAListaDeEspera(primerNodoPedido, ped7);
    agregarPedidoAListaDeEspera(primerNodoPedido, ped8);


    NodoListaRepartidor* nuevoNodoRep = returnRepartidorYCrearSiNoExiste(primerNodoRepartidor, 12313123);
    agregarPedidoARepartidor(nuevoNodoRep, sacarPedidoPorZonaYTransporte(primerNodoPedido, ped1.zona, returnTransporteByVolumenPedido(ped1.volumen)));
    //agregarPedidoARepartidor(nuevoNodoRep, sacarPedidoPorZonaYTransporte(primerNodoPedido, ped2.zona, returnTransporteByVolumenPedido(ped2.volumen)));
    agregarPedidoARepartidor(nuevoNodoRep, sacarPedidoPorZonaYTransporte(primerNodoPedido, ped3.zona, returnTransporteByVolumenPedido(ped3.volumen)));

    nuevoNodoRep = returnRepartidorYCrearSiNoExiste(primerNodoRepartidor, 34534534);
    agregarPedidoARepartidor(nuevoNodoRep, sacarPedidoPorZonaYTransporte(primerNodoPedido, ped4.zona, returnTransporteByVolumenPedido(ped4.volumen)));
    agregarPedidoARepartidor(nuevoNodoRep, sacarPedidoPorZonaYTransporte(primerNodoPedido, ped5.zona, returnTransporteByVolumenPedido(ped5.volumen)));

    nuevoNodoRep = returnRepartidorYCrearSiNoExiste(primerNodoRepartidor, 23242123);
    agregarPedidoARepartidor(nuevoNodoRep, sacarPedidoPorZonaYTransporte(primerNodoPedido, ped6.zona, returnTransporteByVolumenPedido(ped6.volumen)));
    //agregarPedidoARepartidor(nuevoNodoRep, sacarPedidoPorZonaYTransporte(primerNodoPedido, ped7.zona, returnTransporteByVolumenPedido(ped7.volumen)));
    //agregarPedidoARepartidor(nuevoNodoRep, sacarPedidoPorZonaYTransporte(primerNodoPedido, ped8.zona, returnTransporteByVolumenPedido(ped8.volumen)));

}

#endif



