#ifndef REPARTIDOR_ARCHIVO_H
#define REPARTIDOR_ARCHIVO_H

#include <iostream>

using namespace std;

struct RepartidorArchivo
{
    int zona;
    unsigned dni;
    char nombre[50];
    char apellido[50];
    char patente[20];
};

#endif