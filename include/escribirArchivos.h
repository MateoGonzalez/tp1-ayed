#ifndef ESCRIBIR_ARCHIVOS_H
#define ESCRIBIR_ARCHIVOS_H

#include "Repartidor.h"

void escribirArchivos(Repartidor[][80], int zona, string transporte);
void borrarDataArchivos();
void crearArchivos();
#endif