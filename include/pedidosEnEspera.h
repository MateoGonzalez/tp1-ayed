#ifndef PEDIDOS_H
#define PEDIDOS_H

#include "Pedido.h"
#include "Repartidor.h"

void ingresarPedidos(NodoListaPedido *& , Repartidor [][80]);
void agregarPedidoAListaDeEspera(NodoListaPedido*& primerNodoPedido, Pedido pedido);
Pedido sacarPedidoPorZonaYTransporte(NodoListaPedido*& primerNodoPedido, int zona, string transporte);
Pedido sacarPrimerPedidoEnEspera(NodoListaPedido*& primerNodoPedido);


#endif