#ifndef ARBOL_COMERCIOS_H
#define ARBOL_COMERCIOS_H

struct Comercio{
    int codigo;
    int cantidad;
};

struct NodoComercio{
    Comercio comercio;
    NodoComercio* izq;
    NodoComercio* der;
};

#include "Pedido.h"

void crearArbolComercios(NodoListaRepartidor*& primerNodoRepartidor);
void insertarCodigoComercio(NodoComercio*& raizNodoComercio, int codigoComercio);

#endif