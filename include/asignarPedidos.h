#ifndef ASIGNAR_PEDIDOS_H
#define ASIGNAR_PEDIDOS_H

#include "./Repartidor.h"
#include "./Pedido.h"

void asignarPedidos(Repartidor [][80], NodoListaPedido*&, NodoListaRepartidor*&);
NodoListaRepartidor* returnRepartidorYCrearSiNoExiste(NodoListaRepartidor*& primerRepartidor, unsigned dniRepartidor);
void agregarPedidoARepartidor(NodoListaRepartidor *&, Pedido );



#endif