#ifndef MOSTRAR_PEDIDOS_H
#define MOSTRAR_PEDIDOS_H

#include "./Repartidor.h"
#include "./Pedido.h"

void mostrarPedidosEntregados(Repartidor [][80], NodoListaRepartidor*&);

#endif