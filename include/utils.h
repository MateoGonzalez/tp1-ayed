#ifndef UTILS_H
#define UTILS_H

#include "Repartidor.h"
#include "RepartidorArchivo.h"
#include "Pedido.h"

// Nodos
NodoListaPedido* sacarPrimerNodoPedidoDeListaDeRepartidor(NodoListaPedido*& primerNodoPedido);
NodoListaRepartidor* sacarPrimerNodoRepartidor(NodoListaRepartidor*& primerNodoRepartidor);

// Matriz Repartidores
void inicializarMatrizRepartidores(Repartidor[][80], int filas, int colum);
void verMatriz(Repartidor[][80], int filas, int colum);
void agregarRepartidorMatriz(Repartidor [][80], Repartidor, int colum, int zona);

// Repartidores
bool returnEspacioDisponible(Repartidor [][80], int colum, int zona, string transporte);
int returnZonaRepartidorByDni(Repartidor[][80], unsigned dni);
Repartidor returnRepartidorByDni(Repartidor[][80], unsigned dni);
bool returnRepartidorExisteDni(Repartidor[][80], int filas, int colum, unsigned dni);

// Pedidos
string returnTransporteByVolumenPedido(float);
bool returnExisteRepartidorConZonaYTransporte(Repartidor matrizRepartidores[][80], int zona, string transporte);

// validacion utils
bool validarFormatoPatente(string cadena);
void clearTerminal();
bool isNumber(string&);
bool charIsNum(string x);
bool isNumOrFloat(string);


#endif


//bool returnPatenteExiste(Repartidor[][80], int filas, int colum, string patente);