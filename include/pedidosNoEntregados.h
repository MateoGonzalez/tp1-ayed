#ifndef PEDIDOS_NO_ENTREGADOS_H
#define PEDIDOS_NO_ENTREGADOS_H

#include "Pedido.h"
#include "Repartidor.h"

void escribirArchivoConPedidosNoEntregados(NodoListaPedido*& primerNodoPedido);

#endif