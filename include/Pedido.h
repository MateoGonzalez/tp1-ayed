
#ifndef PEDIDO_H
#define PEDIDO_H

#include<iostream>

using namespace std;

struct Pedido {
    string domicilio;
    int zona;
    float volumen;
    float importe;
    int codigoComercio;
};

struct NodoListaPedido {
    Pedido pedido;
    NodoListaPedido* proxNodoPedido;
};

struct NodoListaRepartidor{
    int dniRepartidor;
    NodoListaPedido* primerNodoPedido;

    NodoListaRepartidor* proxNodoRepartidor;
};

#endif
