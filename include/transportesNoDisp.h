#ifndef TRANSPORTES_NO_DISPONIBLES_H
#define TRANSPORTES_NO_DISPONIBLES_H

#include<iostream>

#include "Repartidor.h"

void informarTransportesNoDisponibles(Repartidor [][80], int filas, int colum);

#endif